import requests
from selenium import webdriver
import time
series=[]
start=time.time()
# The JavaScript that we want to inject.
# This updates the second `span` with the execution time of the script.
# `arguments[0]` is how Selenium passes in the callback for `execute_async_script()`.
injected_javascript = (
    'const time = Date.now();'
    'document.getElementById("sphomepage-slider3").innerHTML = ws_key+"";'
    'const callback = arguments[0];'
    'const handleDocumentLoaded = () => {'
    'grecaptcha.execute("6LdMcKwaAAAAABaU3W61LYuBY7fT3NZ436Ndiipc",{action: "getCarInfo"}).then(function(token){document.getElementById("header_car").innerHTML = token+""; });'
    '};'
    'if (document.readyState === "loading") {'
    '  document.addEventListener("DOMContentLoaded", handleDocumentLoaded);'
    '} else {'
    '  handleDocumentLoaded();'
    '}'
)
driver = webdriver.Firefox(executable_path='.\\geckodriver.exe')
driver.get('https://vidange.tn/387-filtre-a-air?order=product.price.asc')
last_token='Aucun véhicule séléctionné'
f1=open('LastMatricule.cfg','r')
f2=open('MaxMatricule.cfg','r')
startMatricule=f1.readline()
max=f2.readline()
f2.close()
print('starting from: '+str(int(startMatricule.split(';')[1])+1)+'TU'+str(int(startMatricule.split(';')[0])))
for matricule in range(int(startMatricule.split(';')[0]),int(max)+1):
    if(matricule==int(startMatricule.split(';')[0])):
        startEnregistrement=int(startMatricule.split(';')[1])+1
    else:
        startEnregistrement=1
    for enregistrement in range(startEnregistrement,10000):
        serie=str(enregistrement)+'TU'+str(matricule)
        driver.execute_script(injected_javascript)
        token=driver.find_element("xpath","//*[@id='header_car']")
        while(token.text==last_token):
            token=driver.find_element("xpath", "//*[@id='header_car']")
        last_token=token.text
        ws_key=driver.find_element("xpath", "//*[@id='sphomepage-slider3']")
        url='https://vidange.tn/api/gatewayclient/registration/'+serie+'?vinverif=3C0KZ48658A300326'
        x = requests.get(url, headers={'Authorization':'Basic '+ws_key.text,'X-Requested-With':'XMLHttpRequest','captcha-token':token.text})
        print(x.text)
        while(x.text==''):
            driver.get('https://vidange.tn/387-filtre-a-air?order=product.price.asc')
            last_token='Aucun véhicule séléctionné'
            driver.execute_script(injected_javascript)
            token=driver.find_element("xpath", "//*[@id='header_car']")

            while(token.text==last_token):
                token=driver.find_element("xpath", "//*[@id='header_car']")
            last_token=token.text
            ws_key=driver.find_element("xpath", "//*[@id='sphomepage-slider3']")
            
            url='https://vidange.tn/api/gatewayclient/registration/'+serie+'?vinverif=3C0KZ48658A300326'
            x = requests.get(url, headers={'Authorization':'Basic '+ws_key.text,'X-Requested-With':'XMLHttpRequest','captcha-token':token.text})
            print(serie)
            print(token.text)
        f=open('voitures.txt','a')
        f.write(x.text.replace('"registration":null','"registration":"'+serie+'"').replace('"code":','"registration":'+serie+'","code":')+'\n')
        f.close()
        f1=open('LastMatricule.cfg','w')
        f1.write(str(matricule)+';'+str(enregistrement))
        f1.close()
driver.quit()
end=time.time()
print(end-start)